#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n2488120\n317050")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "1:\n3.7\n3.5\n3.6\n4.2\n3.7\n0.77\n")

    def test_eval_2(self):
    	r = StringIO("5153:\n2089070\n977564\n919381\n2106936\n1805859\n5154:\n1119079")
    	w = StringIO()
    	netflix_eval(r, w)
    	self.assertEqual(w.getvalue(), "5153:\n2.8\n3.0\n3.2\n2.8\n3.2\n5154:\n3.7\n1.44\n")

    def test_eval_3(self):
    	r = StringIO("5155:\n5156:\n2419618\n1242847\n2183965\n2390123\n5166:")
    	w = StringIO()
    	netflix_eval(r, w)
    	self.assertEqual(w.getvalue(), "5155:\n5156:\n4.0\n3.8\n3.9\n3.7\n5166:\n0.99\n")

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
